### 强制默认头像

#### 维护说明

本插件由 Discuz! X 社区开发者团队维护，您可以在我们的 Gitee 仓库 https://gitee.com/popcorner/overrideavatar 上向我们反馈插件 Bug 。

#### 授权协议

本插件使用[CC0开源协议](./LICENSE)发布，意味着本代码已进入公共领域，您可以随意使用其代码。

#### 插件说明

部分站点在特殊时期会有较大的管理压力，一些注册机发布的不和谐头像为境内的站点带来了较大的隐患。

为保障特殊时期的站点安全，避免被关站，应有关站长的需求而设计此插件。

开启本插件以后所有的头像将会变成默认头像。

更多的兜底安全策略请访问 https://gitee.com/Discuz/DiscuzX/pulls/1049 并了解。

#### 插件用法

开启插件即可，无需做任何设置。关闭插件即可恢复到原始状态。

如果遇到问题，建议关闭其他跟头像有关的插件并再试。