<?php

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class pluginbase_overrideavatar {
	function avatar($value) {
		global $_G;
		$size = empty($value['param'][1]) ? 'middle' : $value['param'][1];
		$returnsrc = empty($value['param'][2]) ? FALSE : $value['param'][2];
		$ucenterurl = empty($value['param'][5]) ? $_G['setting']['ucenterurl'] : $value['param'][5];
		$file = $ucenterurl.'/images/noavatar'.(version_compare(DISCUZ_VERSION, 'X3.5', '<') ? '_'.$size.'.gif' : '.svg');
		$_G['hookavatar'] = $returnsrc ? $file : '<img src="'.$file.'" />';
	}
}

class plugin_overrideavatar extends pluginbase_overrideavatar {}
class mobileplugin_overrideavatar extends pluginbase_overrideavatar {}